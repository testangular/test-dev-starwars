import { Component, OnInit } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    jQuery(window).scroll(function() {
      const offset = jQuery(window).scrollTop();
      jQuery('.navbar').toggleClass('trans', offset > 50);
    });
  }

}
